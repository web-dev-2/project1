"use strict";

function startGamebtn(event)
{
    event.preventDefault();

    let pname = document.getElementById("name").value;
    let bsize = parseInt(document.getElementById("size").value);
    let scolour = document.getElementById("colour").value;
    let diff = document.getElementById("difficulty").value;
    let count = 0;

    disableInputs(true, false);

    createTable(bsize,diff,scolour,count);

    //Regenerates table if there are no squares of the chosen color
    while(count == 0){
        createTable(bsize,diff,scolour,count);
    }
}

//Creates the colour table
function createTable(bsize, diff, scolour, count) {
    count = 0;

    if (document.getElementById('tableHere').children.length != 0) {
        document.getElementById('tableHere').textContent = undefined;
        document.getElementById('tableHere').style.backgroundColor = "grey";
    }

    const tableHere = document.getElementById('tableHere');

    let table = document.createElement('table');
    table.classList.add('cheatcodeOff');
    tableHere.appendChild(table);

    for (let i = 0; i < bsize; ++i) {
        let tr = document.createElement('tr');

        for (let j = 0; j < bsize; ++j) {
            let cell = document.createElement('td');
            tr.appendChild(cell);
            let tdDiv = document.createElement('div');
            cell.appendChild(tdDiv);

            let diffValue = diffp(diff);
            let color = generateColor(diffValue);

            cell.style.backgroundColor = `rgb(${color})`;
            let largestColorNum = highestValue(color);

            if (largestColorNum === scolour) {
                count++;
                cell.classList.add('correct');
            }
            tdDiv.textContent = `(${color}) ${largestColorNum}`;
        }
        table.appendChild(tr);
    }
    document.getElementById("message").textContent = `There are ${count} ${scolour} boxes.`;
}

//Gets the generation number based on difficulty
function diffp(diff)
{
    let num;
    if (diff == 1)
    {
        num = 80;
    }
    else if(diff == 2)
    {
        num = 40;
    }
    else if(diff == 3)
    {
        num = 10;
    }
    else {
        num = 255;
    }
    return num;
}

//Generates random td number
function genRandNum(min, max) {
    return Math.random() * (max - min) + min;
}

//Generates td color based on difficulty
function generateColor(diff)
{
    let min = Math.random(0, 255-diff);
    let max = min + diff;
    const red =  Math.floor(genRandNum(min, max));
    const green = Math.floor(genRandNum(min, max));
    const blue = Math.floor(genRandNum(min, max));

    return `${red}, ${green}, ${blue}`;
}

function highestValue(color)
{
    let arr= color.split(',');
    let hv=Math.max(... arr);
    let hvName="";
    if(hv===Number(arr[0]))
    {
        hvName = "Red";
    }
    else if(hv === Number(arr[1]))
    {
        hvName = "Green";
    }
    else{
        hvName = "Blue";
    }
    return hvName;
}

//Calculates player's score & creates player object to send to the local storage and screen
//Also makes clear button visible
function submitGuess(event){
    disableInputs(false,true);
    let diff = document.getElementById("difficulty").value;
    let bsize = parseInt(document.getElementById("size").value);
    let scolour = document.getElementById("colour").value;

    let correct = getNumCorrect();
    let selected = getNumSelected();
    let pscore = getScore(correct, selected, bsize, diff);
    let pname = document.getElementById("name").value;

    const clearBtn = document.getElementById("clearScores");
    clearBtn.style.display = "inline";

    let player = createPlayer(pname, pscore);
    sendToStorage(player);
    sendToScreen();
}

//Disables or enables buttons
function disableInputs(word1, word2)
{

    document.getElementById("name").disabled = word1;
    document.getElementById("size").disabled = word1;
    document.getElementById("colour").disabled = word1;
    document.getElementById("difficulty").disabled = word1;
    document.getElementsByTagName("button")[0].disabled = word1;
    document.getElementById("submit").disabled = word2;
}

//Retunrs the number of correct answers
function getNumCorrect(){
    let correctResult = 0;
    let clickedTd = Array.from(document.getElementsByClassName('tdBorder'));
    clickedTd.forEach(td => {
        if(td.classList.contains('correct'))
            {
                correctResult++;
            }
            
    });
    return correctResult;
    
}

//Returns the number of tds selected by player
function getNumSelected(){
    let clickedTd = Array.from(document.getElementsByClassName('tdBorder'));
    return clickedTd.length;
}