"use strict";

//Variables for the highscore players & highscore reorder
let players = [];
let reorder = false;

//Calculates score
function getScore(numCorrect, numSelected, boardSize, difficulty) {
    const percent = ( 2 * numCorrect - numSelected ) / (boardSize * boardSize);
    return Math.floor(percent * 100 * boardSize * (difficulty + 1));
   }

//Deletes & creates table to print the players scores in correct order
function sendToScreen(){
    const table = document.getElementById("scoresTable");
    table.remove();

    const newTable = document.createElement('table');
    newTable.setAttribute("id", "scoresTable");

    const headerRow = document.createElement('tr')
    headerRow.setAttribute("id", "columnHeaders");

    const nameHeader = document.createElement('td')
    nameHeader.textContent = "Player name";
    const scoreHeader = document.createElement('td')
    scoreHeader.textContent = "Score";

    headerRow.appendChild(nameHeader);
    headerRow.appendChild(scoreHeader);

    newTable.appendChild(headerRow);

    players.forEach((p) => 
    {
        const newRow = document.createElement('tr');
        const nameCell = document.createElement('td');
        const scoreCell = document.createElement('td');

        nameCell.textContent = players[p].name;
        scoreCell.textContent = players[p].score;

        newRow.appendChild(nameCell);
        newRow.appendChild(scoreCell);

        newTable.appendChild(newRow);
    });
    const tableDiv = document.getElementById("tableScores");
    tableDiv.appendChild(newTable);
}

//Gets the players from storage to print 
function getPlayersFromLocalStorage(){
    if(localStorage.length == 0){
        return;
    }
    else {
        for(let i = 1; i <= localStorage.length; i++){
            const p = JSON.parse(localStorage.getItem(i));
            console.log(p.name);
            //createPlayer(p.name, p.score);
        }
    }
}

//Sends a given player to the local storage
function sendToStorage(player){
    const p = JSON.stringify(player);

    if(localStorage.length == 10){
        let lowestScore = 10000;
        let worstPlayer;
        for(let p in players){
            if(players[p].score < lowestScore){
                lowestScore = players[p].score;
                worstPlayer = players[p];
            }
        }
        for(let i = 1; i <= localStorage.length; i++){
            if(localStorage.getItem(i).name == worstPlayer.name){
                localStorage.setItem(i, player);
            }
        }
    }
    else {
        localStorage.setItem(localStorage.length+1, p);
    }
}

//Creates a player object and adds it to the sorted array & validates highscore length
function createPlayer(name, score){
    let newPlayer = {
        name: '',
        score: ''
    }
    newPlayer.name = name;
    newPlayer.score = score;
//
    let playerCount = players.length;

    if(playerCount >= 10){
        let playerSpot = 0;
        players.forEach((p) => {
            if(players[p].score < newPlayer.score){
                playerSpot = p;
            }
        });
        if(playerSpot == 0){
            return;
        }
        else {
            players[playerSpot] = newPlayer;
        }
    }
    else {
        players[playerCount] = newPlayer;
    }

    players.sort( (a, b) => b.score - a.score );

    return newPlayer;
}

//Reorders the highscores on the screen
function reorderScores(){
    if(reorder == false){
        players.sort( (a, b) => a.score - b.score );
        sendToScreen();
        reorder = true;
    }
    else {
        players.sort( (a, b) => b.score - a.score );
        sendToScreen();
        reorder = false;
    }
}
