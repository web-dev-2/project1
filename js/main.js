"use strict";

document.addEventListener("DOMContentLoaded",setup);

function setup()
{
  //Gets the players from local storage when the page is refreshed & prints the to the screen
  getPlayersFromLocalStorage();
  sendToScreen();

  //Disables the buttons
  document.getElementById("submit").disabled = true;
  document.getElementById("start").disabled = true;

  const form = document.getElementById("inputs");
  form.addEventListener("submit",startGamebtn)

  let tablediv = document.getElementById("tableHere");
 
  //Enables the start button once the user types their name & styles it 
  form.addEventListener("keyup",function(e){

   if(document.getElementById("name").value != 0)
   {
      let colour= document.getElementById("colour").value;
      let startBtn = document.getElementById("start");
      startBtn.disabled=false;
      startBtn.style.background=colour;
      startBtn.style.color = "white";
   }
  });

  //Styles the start button when the user changes the color input
  let colourinput = document.getElementById("colour");
  colourinput.addEventListener("change", function(e)
   {
      document.getElementsById("start").style.background = colour.value;
   });

   //Adds the borders on the selected tds
  tablediv.addEventListener('click', function(e)
  {
      e.target.classList.toggle('tdBorder');
  });

//Cheat code
  document.addEventListener('keydown', function(e) 
  {
    if (e.key === "C" && e.target.tagName !== "INPUT"  && e.shiftKey) 
    {
      let table = document.getElementsByTagName('table')[0];
      table.classList.toggle('cheatcodeOff');
    }
  });

  //Calls the submitGuess function in the table.js to send the play to the screen
  const submitBtn = document.getElementById("submit");
  submitBtn.addEventListener("click", submitGuess);

  //Reorders the scores when you click the header
  const hHeader = document.getElementById("highscoreHeader");
  hHeader.addEventListener("click", reorderScores);

  //Clear the scores & hides the button
  const clearBtn = document.getElementById("clearScores");
  clearBtn.addEventListener("click", function (e) {
      localStorage.clear();
      clearBtn.style.display = "none";
      players = [];
      sendToScreen();
  });
}
