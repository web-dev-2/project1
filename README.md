# Project1

## Name
Beth & Anthony's colours game

## Description
This is a colours game where the user inputs a name, colour, board size and chooses a difficulty. The player must then choose the squares that are the most of the chosen color; i.e. 
if they choose Red they must pick the squares that have an RGB value of mostly red. The difficulty changes the brightness of the colors, making it harder to do well.
The player then gets a score, sent to the score board where 10 other high scores will be shown. The scores can be reordered after clicking the High Scores header
